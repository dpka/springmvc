<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
<h3> Offer had been Created Successfully.</h3>

	 
<table border=1>
	
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Email</th>
		<th>Text</th>
	</tr>
	<c:forEach var="row" items="${Offers}">
	<tr>
    <td> ${row.id}</td>
   <td> ${row.name}</td>
   <td> ${row.email} </td>
    <td>${row.text}</td>
    <td><a href="editOffer?id=${row.id}">Edit</a></td>
    <td><button type="button" class="btn btn-secondary">
    <a style="color:black;" onclick="return confirm('Do you want to delete this user?')"
     href="deleteOffer?id=${row.id}">Delete</a>
    </button>
	</c:forEach>

	
		
	</p>
	
	
</table>
<a href="${pageContext.request.contextPath}/">Home</a>
</body>
</html>