<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
   <%--  <%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create offer page</title>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/main.css"> 


</head>
<body>
<h1> hello from create offer</h1>
<!-- <div class="jumbotron"> -->
 <div class="container">
<sf:form action="${pageContext.request.contextPath}/offerCreated" method="post" commandName="offer">
<!-- <table> -->
<!-- <tr> -->
<!-- 	<td>Name</td> -->
<%-- 	<td><sf:input type="text" name="name" path="name" /></td> --%>
<!-- </tr> -->
<!-- <tr> -->
<!-- 	<td>Email</td> -->
<%-- 	<td><sf:input type="text" name="email" path="email" /></td> --%>
<!-- </tr> -->
<!-- <tr> -->
<!-- 	<td>Text</td> -->
<%-- 	<td><sf:input type="text" name="text" path="text"/></td> --%>
<!-- </tr> -->
<!-- <tr> -->
<!-- 				<td><input type="submit" value="Create"/></td> -->
<!-- 			</tr> -->
<!-- </table> -->

<table class="formtable">
<tr><td >Name: </td><td><sf:input  path="name" name="name" type="text" /><br/><sf:errors path="name"></sf:errors></td></tr>
<tr><td >Email: </td><td><sf:input   path="email" name="email" type="text" /><br/><sf:errors path="email"></sf:errors></td></tr>
<tr><td >Your offer: </td><td><sf:textarea   path="text" name="text" rows="10" cols="10"></sf:textarea><br/><sf:errors path="text"></sf:errors></td></tr>
<tr><td> </td><td><input  value="Create advert" type="submit" /></td></tr>
</table>


</sf:form>
</div>
<!-- </div> -->
</body>
</html>