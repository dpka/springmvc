<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring index page</title>
</head>

<%-- <sql:query var="rs" dataSource="jdbc/TestDB">
select id, username, password from testdb
</sql:query>

<sql:setDataSource var="dataSource"/>

<body>
Hello Dipika.
  Session <%=session.getAttribute("name") %>

Session (JSTL) <c:out value="${name}"></c:out>
	<%=request.getAttribute("name") %>
	
	 <h2>Result From Database</h2>
	 <table border=1>
	 <tr>
	 <th>ID</th>
	<th> Username</th>
	<th>Password</th>
	
</tr>

<c:forEach var="row" items="${rs.rows}">
<tr>
<td> ${row.id}</td>
   <td> ${row.username}</td>
  <td>   ${row.password}</td>
</tr>
</c:forEach>
</table>
<h2> Result from after accessing dao and service layer</h2>
<table border=1>

<tr>
<th>ID</th>
<th>Name</th>
<th>Email</th>
<th>Text</th>
</tr>
<c:forEach var="row" items="${offers }">
<tr>
<td>${row.id}</td>
<td> ${row.name}</td>
<td> ${row.email}</td>
<td>${row.text}</td>
</tr>
</c:forEach>
</table> --%>
<h2>After adding another controller</h2>
<a href="${pageContext.request.contextPath}/createOffer"> CreateOffer</a>

</body>
</html>