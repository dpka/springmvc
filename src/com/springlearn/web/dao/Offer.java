package com.springlearn.web.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="offer")
public class Offer {

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	//@Size(min=2,max=20,message="Size is incorrect")
	private String name;
	
	@Column(name="email")
	//@Pattern(regexp="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		//	+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message="Email is not valid")
	private String email;
	
	@Column(name="text")
	private String text;
	
	public Offer() {
		
	}
	public Offer(String name, String email, String text) {
		this.name= name;
		this.email= email;
		this.text= text;
	}
	public Offer(int id, String name, String email, String text) {
		super();
		this.id= id;
		this.name=name;
		this.email= email;
		this.text=text;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id= id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Offer [id=" + id + ", name=" + name + ", email=" + email
				+ ", text=" + text + "]";
	}

}
