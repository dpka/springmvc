package com.springlearn.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springlearn.web.dao.Offer;
import com.springlearn.web.dao.OffersDao;

@Service("OffersService")
public class OffersService {
	
	 private OffersDao offersDao;
	 
	 public OffersService() {
		 System.out.println("Successfully accessed from service layer");
		 
	 }

	 @Autowired
	public void setOffersDao(OffersDao offersDao) {
		this.offersDao = offersDao;
	}
	 
	 public List<Offer> getCurrent(){
		 return offersDao.getOffers();
		 
	 }
	 
	 public boolean create(Offer offer){
			return	offersDao.create(offer);
			}
	 

	 public Offer getOffer(int id) {
		 return offersDao.getOffer(id);
	 }
	 
	 public Offer findId(int id) {
		 return offersDao.getOffer(id);
	 }
	 
	 public boolean editOffer(Offer offer) {
		 return offersDao.update(offer);
	 }
	 
	 public boolean deleteOffer(int id) {
		 return offersDao.delete(id);
	 }

	 
	/* public void throwTestException() {
			offersDao.getOffer(99999);
		}*/
	 
}
