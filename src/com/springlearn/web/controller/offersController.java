package com.springlearn.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springlearn.web.dao.Offer;
import com.springlearn.web.service.OffersService;

@Controller
public class offersController {
	
	
//	@ExceptionHandler(DataAccessException.class)
//	public String handleDatabaseException(DataAccessException ex) {
//		System.out.println(ex.getMessage());
//		return "error";
//	}

	/*
	 * @RequestMapping("/") public String showHome(HttpSession httpSession) {
	 * httpSession.setAttribute("name", "Deepika"); return "Home";
	 * 
	 * }
	 */
	@RequestMapping("/country")
	public String showCountry() {
		return "country";
	}

	private OffersService offersService;

	@Autowired
	public void setOffersService(OffersService offersService) {
		this.offersService = offersService;
	}
	/*
	 * @RequestMapping("/") public String showHome(Model model) { List<Offer> offers
	 * = offersService.getCurrent(); model.addAttribute("offers", offers); return
	 * "Home"; }
	 */

	@RequestMapping("/createOffer")
	public String getCreateOfferPage(Model model) {
		//offersService.throwTestException();
		model.addAttribute("offer", new Offer());
		return "createOffer";
	}

	@RequestMapping(value = "offerCreated", method = RequestMethod.POST)
	public String createOffer(Model model, Offer offer, BindingResult result) {

		/*System.out.println(offer);
		if (result.hasErrors()) {
			List<ObjectError> objectError = result.getAllErrors();
			for (ObjectError objectE : objectError) {
				System.out.println(objectE.getDefaultMessage());
			}
			System.out.println("Error");
			return "createOffer";
		} else {
			System.out.println("no error");
			offersService.create(offer);
			List<Offer> offers = offersService.getCurrent();
			model.addAttribute("Offers", offers);

		}*/
	offersService.create(offer);
	List<Offer> offers= offersService.getCurrent();
	model.addAttribute("Offers", offers);
		return "offerCreated";
	}
	

	@RequestMapping("getOffer")
	public String getOffer(@RequestParam("id") int id, Model model) {

		System.out.println(id);

		model.addAttribute("id", id);
		return "selectedOffer";
	}

	@RequestMapping("editOffer")
	public String editUser(Model model, Offer offer, HttpServletRequest request) {
		offer = offersService.findId(new Integer(request.getParameter("id")));
		System.out.println(offer);
		model.addAttribute("Offer", offer);
		return "editOffer";

	}

	@RequestMapping("View")
	public String listUser(Model model) {
		List<Offer> offers = offersService.getCurrent();
		model.addAttribute("Offers", offers);
		return "offerCreated";
	}

	@RequestMapping("updateOffer")
	public String updateOffer(Model model,Offer offer) {
		offersService.editOffer(offer);
		List<Offer> offers = offersService.getCurrent();
		model.addAttribute("Offers", offers);
	return "offerCreated";
	}
	
	@RequestMapping("deleteOffer")
	public String updateOffer(Model model,int id) {
		offersService.deleteOffer(id);
		List<Offer> offers = offersService.getCurrent();
		model.addAttribute("Offers", offers);
	return "offerCreated";
	}
	
}
