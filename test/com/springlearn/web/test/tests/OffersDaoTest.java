package com.springlearn.web.test.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.springlearn.web.dao.Offer;
import com.springlearn.web.dao.OffersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:com/springlearn/web/config/dao-context.xml","classpath:com/springlearn/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class OffersDaoTest {

	@Autowired
	private OffersDao offersDao;

	@Test
	public void saveOffer() {
		Offer offer = new Offer("sanjay", "email", "This is a test offer.");

		offersDao.createWithSession(offer);
	}
	
	 @Test
		 public void getAllOffers(){
//		 Offer offer = new Offer("sanjay","email", "This is a test offer.");
//		
//		 offersDao.createWithSession(offer);
//		
		 List<Offer> offers = offersDao.getAllOffers();
		 assertEquals("Should be one offer in database.", 11, offers.size());
		 }
	
	@Test
	public void getMessage() {
		assertEquals("value", 1, 1);
	}
}
